package ru.grpc.test.data.container;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Model {

    private List<Candle> candles;
    private Instrument instrument;
    private String interval;
}
