package ru.grpc.test.mapper;

import org.mapstruct.CollectionMappingStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.NullValueCheckStrategy;
import org.mapstruct.NullValuePropertyMappingStrategy;
import ru.grpc.test.data.container.Model;

@Mapper(uses = {
        InstrumentMapper.class,
        CandleMapper.class},
        collectionMappingStrategy = CollectionMappingStrategy.ADDER_PREFERRED,
        nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS,
        nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface ModelMapper {

    Model map(ru.grpc.test.proto.service.Model model);

    ru.grpc.test.proto.service.Model map(Model model);

}
