package ru.grpc.test.mapper;

import org.mapstruct.CollectionMappingStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.NullValueCheckStrategy;
import org.mapstruct.NullValuePropertyMappingStrategy;
import ru.grpc.test.data.container.Candle;
import ru.grpc.test.mapper.common.BigDecimalMapper;

@Mapper(uses = {
        BigDecimalMapper.class,
        InstrumentMapper.class
},
        collectionMappingStrategy = CollectionMappingStrategy.ADDER_PREFERRED,
        nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS,
        nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface CandleMapper {

    Candle map(ru.grpc.test.proto.service.Candle candle);

    ru.grpc.test.proto.service.Candle map(Candle model);

}
