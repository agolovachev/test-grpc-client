package ru.grpc.test.mapper.common;

import org.mapstruct.Mapper;

import java.math.BigDecimal;

@Mapper
public interface BigDecimalMapper {

    default String mapToString(BigDecimal value) {
        if (value == null) {
            return null;
        }
        return value.toPlainString();
    }

    default BigDecimal mapToBigDecimal(String value) {
        if (value == null) {
            return null;
        }
        return new BigDecimal(value);
    }
}
