package ru.grpc.test.mapper;

import org.mapstruct.Mapper;
import ru.grpc.test.data.container.Instrument;

@Mapper
public interface InstrumentMapper {

    Instrument map(ru.grpc.test.proto.service.Instrument instrument);

    ru.grpc.test.proto.service.Instrument map(Instrument model);

}
