package ru.grpc.test.manager;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import ru.grpc.test.data.container.Model;
import ru.grpc.test.service.impl.RestHttp2RequestService;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

@Slf4j
@Component
@RequiredArgsConstructor
public class TestRestHttp2Manager implements ITestManager {

    private final RestHttp2RequestService requestService;

    @Override
    public void test(int requestCount, final Model model) throws ExecutionException, InterruptedException {
        log.info("EXECUTING : rest http 2 test start; request count - {}", requestCount);
        List<Model> resultList = new ArrayList<>(requestCount);

        for (int i = 0; i < requestCount; ++i) {
            var response = requestService.request(model);
            resultList.add(response);
        }
        log.info("EXECUTING : rest http 2 test end; request count - {}", requestCount);
    }

}
