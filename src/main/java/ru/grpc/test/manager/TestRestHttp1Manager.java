package ru.grpc.test.manager;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import ru.grpc.test.data.container.Model;
import ru.grpc.test.service.impl.RestHttp1RequestService;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Component
@RequiredArgsConstructor
public class TestRestHttp1Manager implements ITestManager {

    private final RestHttp1RequestService requestService;

    @Override
    public void test(int requestCount, final Model model) throws ExecutionException, InterruptedException {
        log.info("EXECUTING : rest http 1.1 test start; request count - {}", requestCount);
        List<Model> resultList = new ArrayList<>(requestCount);

        for (int i = 0; i < requestCount; ++i) {
            var response = requestService.request(model);
            resultList.add(response);
        }

        log.info("EXECUTING : rest http 1.1 test end; request count - {}", requestCount);
    }

}
