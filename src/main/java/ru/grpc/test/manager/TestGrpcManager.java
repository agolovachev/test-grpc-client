package ru.grpc.test.manager;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import ru.grpc.test.data.container.Model;
import ru.grpc.test.service.IRequestService;
import ru.grpc.test.service.impl.GrpcRequestService;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

@Slf4j
@Component
@RequiredArgsConstructor
public class TestGrpcManager implements ITestManager {

    private final GrpcRequestService requestService;

    @Override
    public void test(int requestCount, final Model model) throws ExecutionException, InterruptedException {

        log.info("EXECUTING : grpc test start; request count - {}", requestCount);

        CompletableFuture<List<Model>> completableFuture1 = CompletableFuture
                .supplyAsync(() -> send(requestCount, model));
        CompletableFuture<List<Model>> completableFuture2 = CompletableFuture.supplyAsync(() -> send(requestCount, model));
        CompletableFuture<List<Model>> completableFuture3 = CompletableFuture.supplyAsync(() -> send(requestCount, model));
        CompletableFuture<List<Model>> completableFuture4 = CompletableFuture.supplyAsync(() -> send(requestCount, model));


        CompletableFuture.allOf(
                completableFuture1,
                completableFuture2,
                completableFuture3,
                completableFuture4
        ).get();

        log.info("EXECUTING : grpc test end; request count - {}", requestCount);
    }

    private List<Model> send(int requestCount, final Model model) {
        List<Model> resultList = new ArrayList<>(requestCount);

        for (int i = 0; i < requestCount; ++i) {
            var response = requestService.request(model);
            resultList.add(response);
        }
        return resultList;
    }

}
