package ru.grpc.test.manager;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import ru.grpc.test.data.container.Model;
import ru.grpc.test.utils.DataUtils;

@Slf4j
@Component
@RequiredArgsConstructor
public class TestManager implements CommandLineRunner {

    @Value(value = "${test.request-count:0}")
    private Integer requestCount;

    private final TestGrpcManager testGrpcManager;
    private final TestRestHttp2Manager testRestHttp2Manager;
    private final TestRestHttp1Manager testRestHttp1Manager;

    @Override
    public void run(String... args) throws Exception {

        Model model = DataUtils.createTestData();

        testGrpcManager.test(requestCount, model);
//        testRestHttp2Manager.test(requestCount, model);
        testRestHttp1Manager.test(requestCount, model);
    }


}
