package ru.grpc.test.manager;

import ru.grpc.test.data.container.Model;

import java.util.concurrent.ExecutionException;

public interface ITestManager {

    void test(int requestCount, final Model model) throws ExecutionException, InterruptedException;
}
