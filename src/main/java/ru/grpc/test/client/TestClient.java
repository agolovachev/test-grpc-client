package ru.grpc.test.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import ru.grpc.test.data.container.Model;

@FeignClient(
        name = "rest-client", 
        url = "${integration.rest.http1.url}",
        configuration = ClientConfiguration.class)
public interface TestClient {

    @PostMapping(value = "/post",
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<Model> postRequest(Model request);
}
