package ru.grpc.test.utils;

import lombok.experimental.UtilityClass;
import ru.grpc.test.data.container.Candle;
import ru.grpc.test.data.container.Instrument;
import ru.grpc.test.data.container.Model;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

@UtilityClass
public class DataUtils {

    public final String CANDLE_INTERVAL = "5M";
    public final String BASE_CURRENCY = "BTC";
    public final String COUNTER_CURRENCY = "USDT";
    public final int CANDLE_SIZE = 100;

    public Model createTestData() {
        List<Candle> candleList = new ArrayList<>(CANDLE_SIZE);
        IntStream.range(0, CANDLE_SIZE)
                .forEach(value -> candleList.add(createCandle()));

        return Model.builder()
                .candles(candleList)
                .interval(CANDLE_INTERVAL)
                .instrument(createInstrument())
                .build();
    }

    public Candle createCandle() {
        return Candle.builder()
                .close(BigDecimal.valueOf(100.50))
                .open(BigDecimal.valueOf(110.50))
                .high(BigDecimal.valueOf(120.50))
                .low(BigDecimal.valueOf(130.50))
                .volume(BigDecimal.valueOf(100_000))
                .closeDateTime(DateTimeFormatter.ISO_OFFSET_DATE_TIME.format(ZonedDateTime.now()))
                .openDateTime(DateTimeFormatter.ISO_OFFSET_DATE_TIME.format(ZonedDateTime.now().plusMinutes(5)))
                .interval(CANDLE_INTERVAL)
                .build();
    }

    public Instrument createInstrument() {
        return Instrument.builder()
                .base(BASE_CURRENCY)
                .counter(COUNTER_CURRENCY)
                .build();
    }
}
