package ru.grpc.test.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import ru.grpc.test.data.container.Model;
import ru.grpc.test.service.IRequestService;

import java.net.URI;

@Service
@RequiredArgsConstructor
public class RestHttp2RequestService implements IRequestService {

    @Value("${integration.rest.http2.url}")
    private String baseUrl;

    private final RestTemplate restTemplate;

    @Override
    public Model request(Model model) {
        return restTemplate.postForEntity(
                        URI.create(baseUrl + "/post"),
                        model,
                        Model.class)
                .getBody();
    }
}
