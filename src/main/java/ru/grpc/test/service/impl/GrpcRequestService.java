package ru.grpc.test.service.impl;

import lombok.RequiredArgsConstructor;
import net.devh.boot.grpc.client.inject.GrpcClient;
import org.springframework.stereotype.Service;
import ru.grpc.test.data.container.Model;
import ru.grpc.test.mapper.ModelMapper;
import ru.grpc.test.proto.service.TestServiceGrpc;
import ru.grpc.test.service.IRequestService;

@Service
@RequiredArgsConstructor
public class GrpcRequestService implements IRequestService {

    @GrpcClient("test-grpc-service")
    private TestServiceGrpc.TestServiceBlockingStub testServiceClient;

    private final ModelMapper mapper;

    @Override
    public Model request(Model model) {
        var request = mapper.map(model);
        return mapper.map(
                testServiceClient
                        .request(request)
        );
    }
}
