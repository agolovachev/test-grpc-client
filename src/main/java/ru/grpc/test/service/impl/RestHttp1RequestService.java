package ru.grpc.test.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.grpc.test.client.TestClient;
import ru.grpc.test.data.container.Model;
import ru.grpc.test.service.IRequestService;

@Service
@RequiredArgsConstructor
public class RestHttp1RequestService implements IRequestService {

    private final TestClient testClient;

    @Override
    public Model request(Model model) {
        return testClient.postRequest(model).getBody();
    }
}
