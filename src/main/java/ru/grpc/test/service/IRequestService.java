package ru.grpc.test.service;

import ru.grpc.test.data.container.Model;

public interface IRequestService {

    Model request(Model model);
}
